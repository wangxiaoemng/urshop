
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Urs.Data.Domain.Users;

namespace Urs.Services.Authentication.External
{
    public partial class AuthorizeState
    {
        public IList<string> Errors { get; set; }
        public User User { get; set; }
        public string OpenId { get; set; }
        public string Token { get; set; }

        private readonly string _returnUrl;

        public AuthorizeState(string returnUrl, OpenAuthenticationStatus openAuthenticationStatus)
        {
            Errors = new List<string>();
            _returnUrl = returnUrl;
            AuthenticationStatus = openAuthenticationStatus;

            if (AuthenticationStatus == OpenAuthenticationStatus.Authenticated)
                Result = new RedirectResult(!string.IsNullOrEmpty(_returnUrl) ? _returnUrl : "~/");
        }

        public AuthorizeState(string returnUrl, AuthorizationResult authorizationResult)
            : this(returnUrl, authorizationResult.Status)
        {
            Errors = authorizationResult.Errors;
            OpenId = authorizationResult.OpenId;
            User = authorizationResult.user;
            Token = authorizationResult.Token;
        }

        public OpenAuthenticationStatus AuthenticationStatus { get; private set; }

        public bool Success
        {
            get { return (this.Errors.Count == 0); }
        }

        public void AddError(string error)
        {
            this.Errors.Add(error);
        }

        public ActionResult Result { get; set; }
    }
}