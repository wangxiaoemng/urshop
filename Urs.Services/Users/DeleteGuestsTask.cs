﻿using System;
using Urs.Services.Tasks;

namespace Urs.Services.Users
{
    public partial class DeleteGuestsTask : IScheduleTask
    {
        private readonly IUserService _userService;

        public DeleteGuestsTask(IUserService userService)
        {
            this._userService = userService;
        }

        public void Execute()
        {
            var olderThanMinutes = 1440; //TODO move to settings
            _userService.DeleteGuestUsers(DateTime.Now.AddMinutes(-olderThanMinutes));
        }
    }
}
