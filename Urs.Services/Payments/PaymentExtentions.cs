using System;
using System.Collections.Generic;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Configuration;
using Urs.Services.Orders;

namespace Urs.Services.Payments
{
    public static class PaymentExtentions
    {
        public static bool IsPaymentMethodActive(this IPaymentMethod paymentMethod,
            PaymentSettings paymentSettings)
        {
            if (paymentMethod == null)
                throw new ArgumentNullException("paymentMethod");

            if (paymentSettings == null)
                throw new ArgumentNullException("paymentSettings");

            if (paymentSettings.ActivePaymentMethodSystemNames == null)
                return false;
            foreach (string activeMethodSystemName in paymentSettings.ActivePaymentMethodSystemNames)
                if (paymentMethod.PluginDescriptor.SystemName.Equals(activeMethodSystemName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            return false;
        }

        public static decimal CalculateAdditionalFee(this IPaymentMethod paymentMethod, 
            IOrderTotalCalculationService orderTotalCalculationService, IList<ShoppingCartItem> cart,
            decimal fee, bool usePercentage)
        {
            if (paymentMethod == null)
                throw new ArgumentNullException("paymentMethod");
            if (fee <= 0)
                return fee;

            var result = decimal.Zero;
            if (usePercentage)
            {
                var orderTotal = orderTotalCalculationService.GetShoppingCartTotal(cart);
                result = (decimal)((((float)orderTotal) * ((float)fee)) / 100f);
            }
            else
            {
                result = fee;
            }
            return result;
        }
    }
}
