using System;
using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Payments;
using Urs.Data.Domain.Shipping;

namespace Urs.Services.Orders
{
    public partial interface IOrderService
    {
        #region Orders

        Order GetOrderById(int orderId);

        Order GetOrderByCode(string code);

        IList<Order> GetOrdersByIds(int[] orderIds);

        Order GetOrderByGuid(Guid orderGuid);

        void DeleteOrder(Order order);

        IPagedList<Order> GetOrdersByAffiliateId(int affiliateId = 0, DateTime? startTime = null, DateTime? endTime = null,
           List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null, int pageIndex = 0, int pageSize = int.MaxValue);

        IPagedList<Order> SearchOrders(int? userId = null, DateTime? startTime = null, DateTime? endTime = null,
            OrderStatus? os = null, List<int> psIds = null, List<int> ssIds = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool delete = false);

        void InsertOrder(Order order);

        void UpdateOrder(Order order);

        void DeleteOrderNote(OrderNote orderNote);

        int GetOrderCount(int userId, OrderStatus? os);
        decimal GetOrderTotal(int userId, OrderStatus? os);
        #endregion

        #region Orders goods

        OrderItem GetOrderGoodsById(int orderGoodsVariantId);

        OrderItem GetOrderGoodsByGuid(Guid orderGoodsVariantGuid);

        IList<OrderItem> GetAllorderItems(int? orderId,
           int? userId, DateTime? startTime, DateTime? endTime,
           OrderStatus? os, PaymentStatus? ps, ShippingStatus? ss,
           bool loadDownloableGoodssOnly = false);

        void DeleteOrderGoods(OrderItem orderGoods);

        #endregion

        #region Return requests

        void InsertAfterSales(AfterSales returnRequest);

        void UpdateAfterSales(AfterSales returnRequest);
        void DeleteAfterSales(AfterSales returnRequest);

        AfterSales GetAfterSalesById(int returnRequestId);

        IList<AfterSales> GetAfterSalesByOrderId(int orderId);
        IPagedList<AfterSales> SearchAfterSales(int userId,
            int orderGoodsId, AfterSalesStatus? rs,
            int pageIndex, int pageSize);

        int GetRetrunRequestCount(OrderItem op, User user);

        int GetAfterSalesCount(int userId, AfterSalesStatus? rrs);
        #endregion

    }
}
