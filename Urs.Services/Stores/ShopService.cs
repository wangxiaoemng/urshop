using System;
using System.Collections.Generic;
using System.Linq;
using Urs.Core;
using Urs.Core.Caching;
using Urs.Core.Data;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    /// <summary>
    /// ���ؽӿ�
    /// </summary>
    public partial class ShopService : IShopService
    {
        #region Constants 
        private const string SHOP_BY_ID_KEY = "Urs.shop.id-{0}";
        private const string SHOP_PAGE_KEY = "Urs.shop.page-{0}";
        private const string SHOP_PATTERN_KEY = "Urs.shop.";
        #endregion

        #region Fields

        private readonly IRepository<Shop> _shopRepository;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public ShopService(IRepository<Shop> shopRepository,
            ICacheManager cacheManager)
        {
            _shopRepository = shopRepository;
            _cacheManager = cacheManager;
        }

        #endregion


        #region Shop

        /// <summary>
        /// Deletes a shop
        /// </summary>
        /// <param name="shop">Customs shop</param>
        public virtual void DeleteShop(Shop shop)
        {
            if (shop == null)
                throw new ArgumentNullException("shop");

            _shopRepository.Delete(shop);

            _cacheManager.RemoveByPattern(SHOP_PATTERN_KEY);
        }

        /// <summary>
        /// Gets a shop
        /// </summary>
        /// <param name="shopId">The shop identifier</param>
        /// <returns>Shop</returns>
        public virtual Shop GetShopById(int shopId)
        {
            if (shopId == 0)
                return null;

            //string key = string.Format(SHOP_BY_ID_KEY, shopId);
            //return _cacheManager.Get(key, () =>
            //{
            var n = _shopRepository.GetById(shopId);
            return n;
            //});
        }

        public virtual Shop GetShopByCode(string code)
        {
            if (string.IsNullOrEmpty(code))
                return null;

            //string key = string.Format(SHOP_BY_ID_KEY, code);
            //return _cacheManager.Get(key, () =>
            //{
            var n = _shopRepository.Table.Where(q => q.Code == code).FirstOrDefault();
            return n;
            //});
        }

        /// <summary>
        /// Gets all shop
        /// </summary>
        /// <returns>Customs shops</returns>
        public virtual IPagedList<Shop> GetAllShop(int pageIndex = 0, int pageSize = int.MaxValue, bool? published = true)
        {
            //return _cacheManager.Get(string.Format(SHOP_PAGE_KEY, pageIndex), () =>
            //{
            var query = _shopRepository.Table;
            if (published.HasValue)
                query = query.Where(q => q.Published == published.Value);

            var list = new PagedList<Shop>(query, pageIndex, pageSize);
            return list;
            //});
        }

        /// <summary>
        /// Inserts a shop item
        /// </summary>
        /// <param name="shop">Customs shop</param>
        public virtual void InsertShop(Shop shop)
        {
            if (shop == null)
                throw new ArgumentNullException("shop");

            _shopRepository.Insert(shop);

            _cacheManager.RemoveByPattern(SHOP_PATTERN_KEY);

        }

        /// <summary>
        /// Updates the shop item
        /// </summary>
        /// <param name="shop">Customs shop</param>
        public virtual void UpdateShop(Shop shop)
        {
            if (shop == null)
                throw new ArgumentNullException("shop");

            _shopRepository.Update(shop);

            _cacheManager.RemoveByPattern(SHOP_PATTERN_KEY);

        }

        #endregion



    }
}
