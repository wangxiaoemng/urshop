
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    public partial interface ICopyGoodsService
    {
        Goods CopyGoods(Goods goods, string newName, bool isPublished, bool copyImages);
    }
}
