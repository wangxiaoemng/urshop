using System.Collections.Generic;
using Urs.Data.Domain.Common;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Shipping;

namespace Urs.Services.Shipping
{
    public partial interface IShippingService
    {
        IList<IShippingRateMethod> LoadActiveShippingRateComputationMethods();

        IShippingRateMethod LoadShippingRateComputationMethodBySystemName(string systemName);

        IList<IShippingRateMethod> LoadAllShippingRateComputationMethods();




        void DeleteShippingMethod(ShippingMethod shippingMethod);

        ShippingMethod GetShippingMethodById(int shippingMethodId);


        IList<ShippingMethod> GetAllShippingMethods();

        void InsertShippingMethod(ShippingMethod shippingMethod);

        void UpdateShippingMethod(ShippingMethod shippingMethod);

        decimal GetShoppingCartItemWeight(ShoppingCartItem shoppingCartItem);

        decimal GetShoppingCartItemTotalWeight(ShoppingCartItem shoppingCartItem);

        decimal GetShoppingCartTotalWeight(IList<ShoppingCartItem> cart);
        
        GetShippingOptionRequest CreateShippingOptionRequest(IList<ShoppingCartItem> cart, Address shippingAddress);

        GetShippingOptionResponse GetShippingOptions(IList<ShoppingCartItem> cart, Address shippingAddress,
            string allowedShippingRateComputationMethodSystemName = "");
    }
}
