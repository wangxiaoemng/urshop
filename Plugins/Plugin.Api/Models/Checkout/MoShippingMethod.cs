﻿using System.Collections.Generic;
using Urs.Framework.Mvc;

namespace Plugin.Api.Models.Checkout
{
    /// <summary>
    /// 配送方法
    /// </summary>
    public partial class MoShippingMethodList
    {
        public MoShippingMethodList()
        {
            ShippingMethods = new List<MoShippingMethodItem>();
            Warnings = new List<string>();
        }
        /// <summary>
        /// 配送方法
        /// </summary>
        public IList<MoShippingMethodItem> ShippingMethods { get; set; }
        /// <summary>
        /// 警告
        /// </summary>
        public IList<string> Warnings { get; set; }

        #region Nested classes

        public partial class MoShippingMethodItem
        {
            /// <summary>
            /// 系统名称
            /// </summary>
            public string SystemName { get; set; }
            /// <summary>
            /// 名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 费率
            /// </summary>
            public string Fee { get; set; }
            /// <summary>
            /// 是否选中
            /// </summary>
            public bool Selected { get; set; }
        }
        #endregion
    }

    public partial class MoShippingMethod
    {
        /// <summary>
        /// 系统名称
        /// </summary>
        public string ShippingMethodSystemName { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 费率
        /// </summary>
        public string Fee { get; set; }
    }
}