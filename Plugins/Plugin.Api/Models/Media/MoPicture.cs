﻿using Urs.Framework.Mvc;

namespace Plugin.Api.Models.Media
{
    /// <summary>
    /// 图片
    /// </summary>
    public partial class MoPicture 
    {
        /// <summary>
        /// 原图片url
        /// </summary>
        public string BigUrl { get; set; }
        /// <summary>
        /// 图片url
        /// </summary>
        public string NormalUrl { get; set; }
    }
}