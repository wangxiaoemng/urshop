﻿namespace Plugin.Api.Models.Order
{
    /// <summary>
    /// 商品评论对象
    /// </summary>
    public partial class MoReviewRequest 
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// 被评论商品Id
        /// </summary>
        public int GoodsId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 评论内容
        /// </summary>
        public string ReviewText { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public int Rating { get; set; }
    }
}