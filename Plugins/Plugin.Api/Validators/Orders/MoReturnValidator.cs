﻿using FluentValidation;
using Urs.Services.Localization;
using Urs.Framework.Validators;

namespace Plugin.Api.Models.Order
{
    /// <summary>
    /// 退换货
    /// </summary>
    public class MoReturnValidator : BaseUrsValidator<MoReturn>
    {
        public MoReturnValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Action).NotEmpty().WithMessage("退货操作不能为空");

            RuleFor(x => x.Reason).NotEmpty().WithMessage("退货原因不能为空");
        }

    }
}