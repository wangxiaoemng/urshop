﻿using System.ComponentModel;
using Urs.Framework.Mvc;

namespace Urs.Plugin.Weixin.MiniSDK.Models
{
    public class ConfigurationModel : BaseModel
    {
        [DisplayName("AppId")]
        public string AppId { get; set; }

        [DisplayName("AppSecret")]
        public string AppSecret { get; set; }
    }
}