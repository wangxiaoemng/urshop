using Autofac;
using Autofac.Core;
using Urs.Core.Configuration;
using Urs.Core.Data;
using Urs.Core.Infrastructure;
using Urs.Core.Infrastructure.DependencyManagement;
using Urs.Data;
using Urs.Plugin.Shipping.ByWeight.Data;
using Urs.Plugin.Shipping.ByWeight.Domain;
using Urs.Plugin.Shipping.ByWeight.Services;
using Urs.Framework.Infrastructure.Extensions;

namespace Urs.Plugin.Shipping.ByWeight.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, UrsConfig config)
        {
            builder.RegisterType<ShippingByWeightService>().As<IShippingByWeightService>().InstancePerLifetimeScope();

            //data context
            builder.RegisterPluginDataContext<ShippingByWeightObjectContext>("urs_context_shipping_weight_total_zip");

            //override required repository with our custom context
            builder.RegisterType<EfRepository<ShippingByWeightRecord>>().As<IRepository<ShippingByWeightRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("urs_context_shipping_weight_total_zip"))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<ShippingScopeByWeightRecord>>().As<IRepository<ShippingScopeByWeightRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("urs_context_shipping_weight_total_zip"))
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order => 1;
    }
}