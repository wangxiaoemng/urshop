
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsTagMap : UrsEntityTypeConfiguration<GoodsTag>
    {
        public override void Configure(EntityTypeBuilder<GoodsTag> builder)
        {
            builder.ToTable(nameof(GoodsTag));
            builder.HasKey(pt => pt.Id);
            builder.Property(pt => pt.Name).IsRequired().HasMaxLength(400);

            base.Configure(builder);
        }
    }
}