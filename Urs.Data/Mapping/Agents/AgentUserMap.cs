using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Agents;

namespace Urs.Data.Mapping.Agents
{
    public partial class AgentUserMap : UrsEntityTypeConfiguration<AgentUser>
    {
        public override void Configure(EntityTypeBuilder<AgentUser> builder)
        {
            builder.ToTable("AgentUser");
            builder.HasKey(p => p.Id);

            base.Configure(builder);
        }
    }
}
