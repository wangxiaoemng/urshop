﻿namespace Urs.Data.Domain.Users
{
    public enum UserRoleType
    {
        /// <summary>
        /// 普通用户
        /// </summary>
        Ordinary=3,
        /// <summary>
        /// 设计师用户
        /// </summary>
        Designer = 5,
        /// <summary>
        /// 渠道用户
        /// </summary>
        Agentman = 8,
    }
}
