﻿using System.ComponentModel.DataAnnotations;
using Urs.Core;

namespace Urs.Data.Domain.Banners
{
    public partial class BannerItem : BaseEntity
    {
        public virtual int BannerId { get; set; }
        public virtual string Title { get; set; }
        public virtual string SubTitle { get; set; }
        public virtual string Url { get; set; }
        public virtual int PictureId { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual Banner Banner { get; set; }
    }
}
