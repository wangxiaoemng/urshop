﻿namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// 排序
    /// </summary>
    public enum GoodsSortingEnum
    {
        /// <summary>
        /// 默认排序
        /// </summary>
        Position = 0,
        /// <summary>
        /// 名称: A to Z
        /// </summary>
        NameAsc = 5,
        /// <summary>
        /// 名称: Z to A
        /// </summary>
        NameDesc = 6,
        /// <summary>
        /// 价格: Low to High
        /// </summary>
        PriceAsc = 10,
        /// <summary>
        /// 价格: High to Low
        /// </summary>
        PriceDesc = 11,
        /// <summary>
        /// 创建时间 creation date
        /// </summary>
        CreateTime = 15,
        /// <summary>
        /// 按销量升序
        /// </summary>
        SaleVolumeAsc=20,
        /// <summary>
        /// 按销量降序
        /// </summary>
        SaleVolumeDesc = 21,
    }
}