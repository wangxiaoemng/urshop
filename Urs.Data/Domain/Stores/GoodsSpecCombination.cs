using Urs.Core;
namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// 规格组合
    /// </summary>
    public partial class GoodsSpecCombination : BaseEntity
    {
        /// <summary>
        /// SKU
        /// </summary>
        public virtual string Sku { get; set; }
        /// <summary>
        /// 生产编号
        /// </summary>
        public virtual string ManufacturerPartNumber { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public virtual decimal Price { get; set; }
        /// <summary>
        /// 商品重量
        /// </summary>
        public virtual decimal Weight { get; set; }
        /// <summary>
        /// 库存
        /// </summary>
        public virtual int StockQuantity { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public virtual int PictureId { get; set; }
        /// <summary>
        /// 商品
        /// </summary>
        public virtual int GoodsId { get; set; }
        /// <summary>
        /// 属性描述
        /// </summary>
        public virtual string AttributesDescption { get; set; }
        /// <summary>
        /// 多规格
        /// </summary>
        public virtual string AttributesXml { get; set; }
        /// <summary>
        /// 是否允许缺货
        /// </summary>
        public virtual bool AllowOutOfStockOrders { get; set; }

        public virtual Goods Goods { get; set; }
    }
}
