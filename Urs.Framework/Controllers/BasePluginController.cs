﻿
using Microsoft.AspNetCore.Mvc;

namespace Urs.Framework.Controllers
{
    /// <summary>
    /// Base controller for plugins
    /// </summary>
    public abstract class BasePluginController : Controller
    {
    }
}
