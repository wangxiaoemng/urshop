﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using Urs.Core.Caching;
using Urs.Services.Stores;

namespace Urs.Admin.Infrastructure
{
    /// <summary>
    /// Select list helper
    /// </summary>
    public static class SelectListHelper
    {
        /// <summary>
        /// Get category list
        /// </summary>
        /// <param name="categoryService">Category service</param>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Category list</returns>
        public static List<SelectListItem> GetCategoryList(ICategoryService categoryService, ICacheManager cacheManager, bool showHidden = false)
        {
            //string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORIES_LIST_KEY, showHidden);
            //var categoryListItems = cacheManager.Get(cacheKey, () =>
            //{
                var categories = categoryService.GetAllCategories(showHidden: showHidden);
                //return categories.Select(c => new SelectListItem
                var categoryListItems = categories.Select(c => new SelectListItem
                {
                    Text = c.GetCategoryBreadCrumb(categoryService),
                    Value = c.Id.ToString()
                });
            //});

            var result = new List<SelectListItem>();
            //clone the list to ensure that "selected" property is not set
            foreach (var item in categoryListItems)
            {
                result.Add(new SelectListItem
                {
                    Text = item.Text,
                    Value = item.Value
                });
            }

            return result;
        }
    }
}