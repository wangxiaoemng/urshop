﻿using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.ShoppingCart
{
    public partial class ShoppingCartModel : BaseModel
    {
        [UrsDisplayName("Admin.CurrentCarts.User")]
        public int UserId { get; set; }
        [UrsDisplayName("Admin.CurrentCarts.User")]
        public string UserEmail { get; set; }

        [UrsDisplayName("Admin.CurrentCarts.TotalItems")]
        public int TotalItems { get; set; }
    }
}