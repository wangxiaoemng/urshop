﻿using System;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.ShoppingCart
{
    public partial class ShoppingCartItemModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.CurrentCarts.Goods")]
        public int GoodsId { get; set; }

        [UrsDisplayName("Admin.CurrentCarts.Goods")]
        public string FullGoodsName { get; set; }
        public string AttributeInfo { get; set; }

        [UrsDisplayName("Admin.CurrentCarts.UnitPrice")]
        public string UnitPrice { get; set; }
        [UrsDisplayName("Admin.CurrentCarts.Quantity")]
        public int Quantity { get; set; }
        [UrsDisplayName("Admin.CurrentCarts.Total")]
        public string Total { get; set; }
        [UrsDisplayName("Admin.CurrentCarts.UpdatedOn")]
        public DateTime UpdatedOn { get; set; }
    }
}