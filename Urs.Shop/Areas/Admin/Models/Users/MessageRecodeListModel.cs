﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Urs.Framework;

namespace Urs.Admin.Models.Users
{
    public class MessageRecodeListModel
    {
        [UrsDisplayName("Admin.Users.Users.Fields.UserId")]
        public int? UserId { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.Code")]
        public string Code { get; set; }
        [UrsDisplayName("Admin.Orders.List.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartTime { get; set; }
        [UrsDisplayName("Admin.Orders.List.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndTime { get; set; }
        [UrsDisplayName("Admin.Users.Users.List.SearchPhone")]
        
        public string Phone { get; set; }
    }
}