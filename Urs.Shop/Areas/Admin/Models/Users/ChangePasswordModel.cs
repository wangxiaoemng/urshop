﻿using System.ComponentModel.DataAnnotations;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class ChangePasswordModel : BaseModel
    {
        
        [DataType(DataType.Password)]
        [UrsDisplayName("Account.ChangePassword.Fields.OldPassword")]
        public string OldPassword { get; set; }

        
        [DataType(DataType.Password)]
        [UrsDisplayName("Account.ChangePassword.Fields.NewPassword")]
        public string NewPassword { get; set; }

        
        [DataType(DataType.Password)]
        [UrsDisplayName("Account.ChangePassword.Fields.ConfirmNewPassword")]
        public string ConfirmNewPassword { get; set; }

        public string Result { get; set; }

    }
}