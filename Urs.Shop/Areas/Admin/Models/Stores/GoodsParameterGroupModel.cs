﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Urs.Admin.Validators.Stores;
using Urs.Framework;
using Urs.Framework.Localization;
using Urs.Framework.Mvc;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    public partial class GoodsParameterGroupModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Store.ParameterGroups.Fields.Name")]
        
        public string Name { get; set; }

        [UrsDisplayName("Admin.Store.ParameterGroups.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

    }
}