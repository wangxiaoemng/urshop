﻿using Microsoft.AspNetCore.Mvc;
using Urs.Admin.Models.Shipping;
using Urs.Core;
using Urs.Data.Domain.Configuration;
using Urs.Data.Domain.Shipping;
using Urs.Framework.Controllers;
using Urs.Framework.Extensions;
using Urs.Services.Configuration;
using Urs.Services.Directory;
using Urs.Services.Localization;
using Urs.Services.Plugins;
using Urs.Services.Security;
using Urs.Services.Shipping;

namespace Urs.Admin.Controllers
{
    [AdminAuthorize]
    public partial class ShippingController : BaseAdminController
    {
        #region Fields

        private readonly IShippingService _shippingService;
        private readonly ShippingSettings _shippingSettings;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IPluginFinder _pluginFinder;
        private readonly IAreaService _areaService;
        private readonly IWebHelper _webHelper;

        #endregion

        #region Constructors

        public ShippingController(IShippingService shippingService, 
            ShippingSettings shippingSettings,
            ISettingService settingService,  
            ILocalizationService localizationService, 
            IPermissionService permissionService,
            IPluginFinder pluginFinder,
            IWebHelper webHelper,
            IAreaService areaService)
        {
            this._shippingService = shippingService;
            this._shippingSettings = shippingSettings;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._pluginFinder = pluginFinder;
            this._areaService = areaService;
            this._webHelper = webHelper;
        }

        #endregion

        #region Shipping methods


        public IActionResult CreateMethod()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();

            var model = new ShippingMethodModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult CreateMethod(ShippingMethodModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();

            if (ModelState.IsValid)
            {
                var sm = model.ToEntity<ShippingMethod>();
                _shippingService.InsertShippingMethod(sm);
                return continueEditing ? RedirectToAction("EditMethod", new { id = sm.Id }) : RedirectToAction("Methods");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public IActionResult EditMethod(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();
            var model = new ShippingMethodModel();
            var sm = _shippingService.GetShippingMethodById(id);
            if (sm == null)
            {
                return View(model);
            }
            else
            {
                model = sm.ToModel<ShippingMethodModel>();
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult EditMethod(ShippingMethodModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();

            var sm = _shippingService.GetShippingMethodById(model.Id);
            if (sm == null)
            {
                var sms = model.ToEntity<ShippingMethod>();
                _shippingService.InsertShippingMethod(sms);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    sm = model.ToEntity(sm);
                    _shippingService.UpdateShippingMethod(sm);
                }
            }
            return Json(new { success = 1 });
        }

        [HttpPost]
        public IActionResult DeleteMethod(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return HttpUnauthorized();

            var sm = _shippingService.GetShippingMethodById(id);
            if (sm == null)
                //No shipping method found with the specified id
                return RedirectToAction("Methods");

            _shippingService.DeleteShippingMethod(sm);
            return RedirectToAction("Methods");
        }

        #endregion
    }
}
